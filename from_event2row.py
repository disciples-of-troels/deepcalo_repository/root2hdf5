import h5py
import glob
from tqdm import tqdm
from collections import OrderedDict
import os
from os import listdir
from os.path import isfile, join
import numpy as np
import time
import itertools
import pandas as pd
import hdfdict
import random
import matplotlib.pyplot as plt
from collections import Counter
import seaborn as sns
import concurrent
from sklearn.model_selection import train_test_split
import argparse
from numba import njit
import sys
from skhep.math import vectors
from variableLists import keep_branchesMC, GNN_variables

"""

python from_event2row.py --outdir output/root2hdf5/Hyy_mc_time_gain_combine/ --train_test_valid 0.7,0.1,0.2 --datatype MC --tag Hyy output/root2hdf5/Hyy_mc_raw/

"""
#%%
# @njit()
def reconstruct_E_truth(eventNumber, p_eta, p_phi, p_e, number_of_decay_particles=2):
    if number_of_decay_particles != None:
        duplicates = [item for item, count in Counter(eventNumber).items() if count == number_of_decay_particles]
        mask_reco = np.in1d(eventNumber, duplicates)
        eventNumber_iter = list(dict.fromkeys(eventNumber[mask_reco]))
        reco_mass = np.zeros((eventNumber.shape))
        reco_energy = np.zeros((eventNumber.shape))
        for evtnr in tqdm(eventNumber_iter, total = len(eventNumber_iter)): #zip(mask_reco, eventNumber):#
            evt_mask  = eventNumber == evtnr
            if np.sum(evt_mask) != 2: continue
            eta=p_eta[evt_mask]
            phi=p_phi[evt_mask]
            e=p_e[evt_mask]
            k = (np.cosh(eta[0]-eta[1])-np.cos(phi[0]-phi[1]))
            energy0 = 91**2*np.prod(np.cosh(eta))/(2*k*e[1])
            energy1 = 91**2*np.prod(np.cosh(eta))/(2*k*e[0])
            mass = np.sqrt(2 * np.prod(e)/np.prod(np.cosh(eta)) * k)
            reco_energy[evt_mask] = [energy0, energy1]
            reco_mass[evt_mask] = [mass, mass]
        
    else:
        eventNumber_iter = np.unique(eventNumber)
        reco_mass = []#np.zeros((eventNumber.shape))
        reco_energy = []#np.zeros((eventNumber.shape))
        eta_loop = np.hstack([(p_eta[::2], p_eta[1::2])]).T
        phi_loop = np.hstack([(p_phi[::2], p_phi[1::2])]).T
        e_loop = np.hstack([(p_e[::2], p_e[1::2])]).T
        mask = []
        for eta, phi, e in tqdm(zip(eta_loop, phi_loop, e_loop), total=len(eta_loop)):
            k = (np.cosh(eta[0]-eta[1])-np.cos(phi[0]-phi[1]))
            energy = k*e[0]/np.cosh(eta[1])*e[1]/np.cosh(eta[1])*2
            if (np.sqrt(energy) < 97) and (np.sqrt(energy) > 86):
                mask.append([True,True])
            else:
                mask.append([False, False])
            energy0 = 91**2*np.prod(np.cosh(eta))/(2*k*e[1])
            energy1 = 91**2*np.prod(np.cosh(eta))/(2*k*e[0])
            mass = np.sqrt(2 * np.prod(e)/np.prod(np.cosh(eta)) * k)
            reco_energy.append([energy0, energy1])
            reco_mass.append([mass, mass])
    return reco_mass, reco_energy, mask

def invMass(*, energy1, eta1, phi1, energy2, eta2, phi2, energy3=None, eta3=None, phi3=None):
    # make four vector
    vecFour1 = vectors.LorentzVector()
    vecFour2 = vectors.LorentzVector()
    et1 = (np.abs(energy1))/np.cosh(eta1)
    et2 = (np.abs(energy2))/np.cosh(eta2)
    vecFour1.setptetaphim(et1, eta1, phi1, 0) #Units in GeV for pt and mass
    vecFour2.setptetaphim(et2, eta2, phi2, 0)
    
    if energy3!=None:
        et3 = (np.abs(energy3))/np.cosh(eta3)
        vecFour3 = vectors.LorentzVector()
        vecFour3.setptetaphim(et3, eta3, phi3, 0)
        vecFour = vecFour1+vecFour2+vecFour3 
    else:
        vecFour = vecFour1+vecFour2
    
    invM = vecFour.mass
    return invM


def drop_bad_values(f, columns, isMC,particle_type=''):
    global n
    if isMC:
        if 'single' in particle_type:
            true_events_mask = (f['p_truth_e'][:n]>0)
        elif 'Zee' in particle_type: ## electron and/or photons
            true_events_mask = ((f['p_truth_e'][:n]>0) & (f['p_truthOrigin'][:n] == 13) &
                                (np.abs(f['p_truth_pdgId'][:n]) == 11))
        elif 'Zmumugam' in particle_type: # photons Zllgamma ########### for muons only
            true_events_mask = ( (f['p_truth_e'][:n]>0)& ((f['p_truthOrigin'][:n] == 13) | (f['p_truthOrigin'][:n] == 3)) &
                                ((np.abs(f['p_truth_pdgId'][:n])  == 13) | (np.abs(f['p_truth_pdgId'][:n])  == 22)))
        elif 'Hyy' in particle_type: 
            true_events_mask = ((f['p_truth_e'][:n]>0) & ((f['p_truthOrigin'][:n] == 14)) &
                                (np.abs(f['p_truth_pdgId'][:n]) == 22))
        else:
            print('Tag not defined or unknow')
            sys.ext()
    elif ~isMC:
        print('Running data files')
        true_events_mask = f['p_PhotonLHLoose'][:n] | f['p_ElectronLHLoose'][:n] | f['p_MuonLHLoose'][:n]
    else:
        print('Unknown sample type (mc or data)')
        sys.exit()
    return true_events_mask

def create_h5(*, tag:str, datatype: str):
    global outdir

    fname = './'+outdir+f'{tag}_{datatype}_image_{time.time()}.h5'
    with h5py.File(fname, 'w') as df:
        df.create_group('train')
        df.create_group('val')
        df.create_group('test')

    return fname

def event_to_row_parallel(all_input):
    name, columns, folder_tag, sample, isMC, tag = all_input

    text = event_to_row(filenames=name,isMC=isMC, folder=folder_tag, subkeys=columns,
                        train_test_valid=sample, parallel=False, tag=tag)
    return text

def event_to_row(filenames: str,isMC: bool, folder:str, subkeys: list = None,
                 train_test_valid: list = None, parallel=False, tag=None):
    """
    train_test_valid is not working for multiple particle types
    """
    global n
    datatype = 'mc' if isMC else 'data'
    if type(filenames) != list:
        filenames = [filenames]
    if type(train_test_valid) != list:
        train_test_valid=[train_test_valid]
    if tag==None:
        print('tag is None - exiting....')
        sys.exit()

    if (train_test_valid is not None) & ~(parallel):
        assert len(train_test_valid) == len(filenames), 'train_test_valid and filenames not same length'

    path = create_h5(tag=tag, datatype=datatype)

    # old_catg = 'random'
    old_filename = ''
    for catg in train_test_valid:
        for filename in filenames[0]: # make to multicore
            # print('########## ', filename, train_test_valid)
            # if catg != old_catg:
            #     first = True
            with h5py.File(filename, 'r') as f:
                print(filename)
                mask_subkeys = np.in1d(np.hstack(subkeys), list(f.keys()))
                if np.sum(mask_subkeys) != len(np.hstack(subkeys)):
                    print("The data is missing variables: ")
                    print(np.hstack(subkeys)[~mask_subkeys])
                    print(', exiting!')
                    sys.exit()
                with h5py.File(path, 'a') as hf:
                    
                    mask_drop_bad = drop_bad_values(f, subkeys, particle_type=tag, isMC=isMC)[:n]
                    # =============================================================================
                    #                     Numba should be applied for speed - and should not loop over phi, eta and energy
                    # =============================================================================
                    if (~isMC) & (old_filename != filename) & (tag=='Zee'):
                        start_time = time.time()
                        eventNumber = f['eventNumber'][:n][mask_drop_bad]
                        # print('eta')
                        p_eta = f['p_eta'][:n][mask_drop_bad]
                        # print('phi')
                        p_phi = f['p_phi'][:n][mask_drop_bad]
                        # print('energy')
                        p_e = f['p_e'][:n][mask_drop_bad]
                        if (tag=='Zee'):
                            # print('duplicates')
                                                       
                            # print('eta')
                            reco_mass, reco_energy  = reconstruct_E_truth(eventNumber=eventNumber,
                                                                          p_eta=p_eta, p_phi=p_phi,
                                                                          p_e=p_e)
                        elif False:#('Zmumugam' in tag):
                            # not implemented
                            E_T = p_e/np.cosh(p_eta)
                            mask_photon = (
                                            (f['p_PhotonLHTight'][:n][mask_drop_bad])  # using Tight in photons
                                             & ((f['p_ptvarcone20'][:n][mask_drop_bad]+f['p_ptvarcone30'][:n][mask_drop_bad]+f['p_ptvarcone40'][:n][mask_drop_bad])/E_T < 0.3)
                                             & ((f['p_topoetcone20'][:n][mask_drop_bad]+f['p_topoetcone30'][:n][mask_drop_bad]+f['p_topoetcone40'][:n][mask_drop_bad])/E_T < 0.3)
                                            )
                            mask_muon = f['p_MuonLHLoose'][:n][mask_drop_bad]
                            duplicates = [item for item, count in Counter(eventNumber).items() if count >= 3] 
                            mask_true_events = (mask_muon | mask_photon) & (E_T > 9.5) & (duplicates)
                            # number of events

                            
                            # evtnr = all_evtnr[mask_true_events & mask_duplicates]
                        print('Sec:', time.time()-start_time)
                        print(len(np.array(reco_mass)))
                        old_filename = filename
                        var_name_mass = 'ATLAS_mass'
                        var_name_energy = 'ATLAS_91_energy'
                        maxsize = list(reco_mass.shape)
                        maxsize[0] = None
                        try:
                            hf[catg].create_dataset(var_name_mass, data=np.array(reco_mass), chunks=True, maxshape=tuple(maxsize), compression='lzf' )
                            hf[catg].create_dataset(var_name_energy, data=np.array(reco_energy), chunks=True, maxshape=tuple(maxsize), compression='lzf' )
                        except OSError:
                            hf[catg][var_name_mass].resize((hf[catg][var_name_mass].shape[0] + reco_mass.shape[0]), axis = 0)
                            hf[catg][var_name_mass][-reco_mass.shape[0]:] = reco_mass
                            hf[catg][var_name_energy].resize((hf[catg][var_name_energy].shape[0] + reco_energy.shape[0]), axis = 0)
                            hf[catg][var_name_energy][-reco_energy.shape[0]:] = reco_energy
                            
                    for col in subkeys:
                        # print(col)
                        if type(col) == list:
                            array=0
                            for i in col:
                                data = f[i][:n][mask_drop_bad]
                                if f[i].shape[1:] != (56,11):
                                    # print(data.shape)
                                    width = int(data.shape[2])
                                    height = int(data.shape[1]*8)
                                    dim = (height, len(data))
                                    # resize image
                                    # print(dim)
                                    data = cv2.resize(data/8, dsize=dim, interpolation = cv2.INTER_NEAREST)
                                    # print(data.shape)
                                array += data # adding layers together
                            col = col[0]
                        else:
                            if col == 'tile_gap_Lr1':
                                array = np.sum(np.sum(f[col][:n][mask_drop_bad],axis=1), axis=1)
                            else:
                                array = f[col][:n][mask_drop_bad]
                        try:
                            maxsize = list(array.shape)
                            maxsize[0] = None
                            if parallel:
                                if isMC:
                                    train, val = train_test_split(array, test_size=0.3, random_state=42, shuffle=True)
                                    del array
                                    val, test = train_test_split(val, test_size=0.33, random_state=42, shuffle=True)
                                else:
                                    train, val = train_test_split(array, test_size=0.7, random_state=42, shuffle=False)
                                    del array
                                    val, test = train_test_split(val, test_size=0.3, random_state=42, shuffle=False)
                                for set_name, arr in zip(['train', 'test', 'val'], [train, test, val]):
                                    hf[set_name].create_dataset(col, data=arr, chunks=True, maxshape=tuple(maxsize), compression='lzf' )
                            else:
                                # print(catg, col,)
                                # print(path, filename)
                                hf[catg].create_dataset(col, data=array, chunks=True, maxshape=tuple(maxsize), compression='lzf')
                        except (OSError, RuntimeError, ValueError, TypeError):
                            try:
                                hf[catg][f'{col}'].resize((hf[catg][f'{col}'].shape[0] + array.shape[0]), axis = 0)
                                hf[catg][f'{col}'][-array.shape[0]:] = array
                            except TypeError:
                                print('Wow ', filename)
                                print(col, array, filename)
                                continue
    
    
                    # first = False
                    # old_catg = catg
            print(f'Done with {filename}')

def run_event2row(isMC, paths, train_test_valid = [0.6, 0.2, 0.2], single_file=False, tag=None):
    if type(paths) != list:
        paths = [paths]
    if type(train_test_valid) != list:
        paths = [train_test_valid]
    columns = keep_branchesMC()+GNN_variables()
    columns.remove('p_MuonLHTight')
    print(paths)
    for path in paths:
        onlyfiles = [path+f for f in listdir(path) if isfile(join(path, f))]
        

       
    all_onlyfiles = []
    # sort files after the root file.
    file_numbers = [int(i.split('_')[-1].split('.h5')[0]) for i in onlyfiles]
    onlyfiles = np.array(onlyfiles)[np.argsort(file_numbers)]
    _, idx, count = np.unique(file_numbers,return_index=True,return_counts=True)
    for i in range(len(count)):
        all_onlyfiles.append(onlyfiles[i*count[0]:(i+1)*count[0]])
    # all_onlyfiles = all_onlyfiles[:-2]
    all_samples = [int(i*len(count))*[j] for i, j in zip(train_test_valid, ['train', 'test', 'val'])]
    print(all_samples)
    if len(np.hstack(all_samples)) < len(all_onlyfiles):
        for i in range(len(all_onlyfiles)-len(np.hstack(all_samples))):
            all_samples.extend(['train'])
    all_samples = np.hstack(all_samples)
    # all_samples = all_samples[:-2]   
    
    if single_file:
        # print(all_onlyfiles, columns, path, all_samples, isMC)
        event_to_row(filenames=all_onlyfiles, subkeys = columns, folder = path,
                              train_test_valid = all_samples, isMC=isMC, tag=tag)
    else:
        print(all_samples)
        args = [(files, columns, paths[0], sample, isMC, tag) for files, sample in zip(all_onlyfiles, all_samples)]
        cpu_cores = len(all_onlyfiles)
        if False:
            if len(all_onlyfiles) > 25:
                cpu_cores=25
            with concurrent.futures.ProcessPoolExecutor(max_workers=cpu_cores) as executor:
                results = executor.map(event_to_row_parallel, args)
            for i in results:
                print(i)
        else:
            for i in args:
                print(event_to_row_parallel(i))
if __name__ == '__main__':
    # local
    # Command line options
    import cv2
    if os.getcwd() == '/home/malteal/hep/work/root2hdf5':
        datatype = 'MC'
        isMC = True if datatype == 'MC' else False
        train_test_valid = [0.7,0.1,0.2]
        paths = './output/root2hdf5/Zmumumgam_new_raw/'#Zee_mc_time_gain_raw/'
        tag = 'Zmumugam'
        path = './output/root2hdf5/Zmumugam_mc_new_raw/'
        outdir = 'output/root2hdf5/Zmumugam_mc_new_combine/'
        
        n = 10_000
    else:
        parser = argparse.ArgumentParser(description="Flatten HDF5 into row based files and split it up in train-test-validation")
    
        parser.add_argument('--outdir', action='store', default="output/root2hdf5/", type=str,
                            help='Output directory.')
    
        parser.add_argument('--train_test_valid', action='store', required=True , type=str,
                            help='Ratio of files split in to train-test-valid - NO SPACING. Ex: 0.7,0.1,0.2')
    
        parser.add_argument('--datatype', action='store', default='MC', type=str,
                            choices=['MC', 'Data'], help='Real Data or MC')
    
        parser.add_argument('--tag', action='store', type=str, required=True,
                            help='Tag the data category (Zee, Wev, etc.).')
    
        parser.add_argument('paths', type=str, nargs='+',
                            help='ROOT file(s) to be converted.')
    
        args = parser.parse_args()
        path = args.paths
        isMC = args.datatype == 'MC'
        outdir = args.outdir
    n = None
    if os.getcwd() == '/home/malteal/hep/work/root2hdf5':
        run_event2row(isMC=isMC, paths= paths, train_test_valid = train_test_valid, single_file =False, tag=tag)
    else: # cluster
        try:
            train_test_valid = [float(i) for i in args.train_test_valid.split(',')]
            print(train_test_valid)
        except:
            print('Did you write the train_test_valid ratio correct?')
            sys.exit()
        run_event2row(isMC=isMC, paths= args.paths, train_test_valid = train_test_valid, single_file =False, tag=args.tag)
