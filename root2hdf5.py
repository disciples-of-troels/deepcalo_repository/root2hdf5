#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''

    python -u root2hdf5.py --tag Zmumugam --tree tree --datatype MC ../ntupleproduction/run/output/Zmumugam_mc_EGAM4/*
'''


print("Program running...")


import sys
import h5py
import numpy as np
import logging as log
import multiprocessing
import ROOT
import argparse
import gc
import os, glob
from time import time
from datetime import timedelta
from tqdm import tqdm
# from from_event2row import run_event2row
from utils import mkdir
from variableLists import GNN_variables, remove_branches_data, keep_branchesMC
from numba import njit

import warnings
warnings.filterwarnings('ignore', 'ROOT .+ is currently active but you ')
warnings.filterwarnings('ignore', 'numpy .+ is currently installed but you ')
warnings.filterwarnings('ignore')
#%%
# Logging style and levelo
log.basicConfig(format='[%(levelname)s] %(message)s', level=log.INFO)
log.info("Packages imported")

# Start "timer"
t_start = time()

# Command line options
parser = argparse.ArgumentParser(description="Convert ROOT files with nested structure into flat HDF5 files.")
parser.add_argument('--stop', action='store', default=None, type=int,
                    help='Maximum number of events to read.')
parser.add_argument('--max-processes', action='store', default=30, type=int,
                    help='Maximum number of concurrent processes to use.')
parser.add_argument('--outdir', action='store', default="output/root2hdf5/", type=str,
                    help='Output directory.')
parser.add_argument('--max-input-files', action='store', default=0, type=int,
                    help='Use at most X files. Default is all.')
parser.add_argument('--selection', action='store', default=None, type=str,
                    help='selection string on tree')
parser.add_argument('--datatype', action='store', default='MC', type=str,
                    choices=['MC', 'Data'], help='Real Data or MC')
local_test= False

if not local_test:
    parser.add_argument('--tag', action='store', type=str, required=True,
                        help='Tag the datas category (Zee, Wev, single, etc.).')
    parser.add_argument('--tree', action='store', required=True, type=str,
                        help='Name of the tree to save!')
    parser.add_argument('paths', type=str, nargs='+',
                        help='ROOT file(s) to be converted.')

args = parser.parse_args()
if local_test:
    args.paths = [
                  # '../ntupleproduction/run/output/Zmumugamma_data_EGAM4/data16_13TeV_Zmumugamma_EGAM4_calocells_0.root'
                  # '../ntupleproduction/run/output/Zee_data_EGAM1/data16_13TeV_Zee_EGAM1_calocells_0.root'
                  '../ntupleproduction/run/output/Zee/Zee_mc_all_time_gain/mc16_13TeV_Zee_EGAM1_calocells_1_10.root'
                  # '../ntupleproduction/run/output/mc_single_electron/mc16_13TeV_Zee_EGAM1_calocells_78.root'#Zee/Zee_data_EGAM1/data16_13TeV_Zee_EGAM1_calocells_1.root'
                  ]#['../ntupleproduction/run/output/Zllgam_mc/mc16_13TeV_Zmumugamma_EGAM4_10_35_calocells_0_2_.root'] # skal måske være en liste
    args.tag = 'Zee'
    args.tree ='tree'

# Validate arguments
if not args.paths: # args.paths
    log.error("No ROOT files were specified.")
    quit()
elif '*' in args.paths:
    folder, ftype = args.paths.split('*')
    os.chdir(folder)
    args.paths = []
    for file in glob.glob("*"+ftype):
        args.paths.append(file)
    print(args.paths)
#%%
# if args.max_processes > 35:
#     log.error("The requested number of processes ({}) is excessive (>31). Exiting.".format(args.max_processes))
#     quit()

# Maximum number of events
if args.stop is not None:
    args.stop = int(args.stop)
else:
    args.stop = 100_000_000

# Standard selection is no two tags, and tag must be electron (this doesn't work for background or dumps which have no tags)
if args.selection == None:
    args.selection = ""

# Make and set the output directory to tag, if it doesn't already exist
# Will stop if the output already exists since re-running is either not needed or unwanted
# If it's wanted, delete the output first yourself
args.outdir = args.outdir+args.tag+"/"
if os.path.exists(args.outdir):
    log.error("Output already exists - please remove yourself.")
    # quit()
else:
    mkdir(args.outdir)

# Sort paths, because it's nice
# args.paths = sorted(args.paths)

# File number counter (incremented first in loop)
counter = -1


#============================================================================
# Functions
#============================================================================

def remove_bad_events(data):
    tag = args.tag.split('_')[0]

    if args.datatype == 'MC':
        # print('MC')
        if 'single' in tag:
            mask_remove_999_energy = (data['p_truth_e']>0)
        elif 'Zee' in tag: ## electron and/or photons
            mask_remove_999_energy = ((data['p_truth_e']>0) & (data['p_truthOrigin'] == 13) &
                                (np.abs(data['p_truth_pdgId']) == 11))
        elif 'Zmumugam' in tag: # photons Zllgamma ########### for muons only
            mask_remove_999_energy = ( (data['p_truth_e']>0)& ((data['p_truthOrigin'] == 13) | (data['p_truthOrigin'] == 3)) &
                                ((np.abs(data['p_truth_pdgId'][:])  == 13) | (np.abs(data['p_truth_pdgId'])  == 22)))

        elif 'Hyy' in tag:
            mask_remove_999_energy = ((data['p_truth_e']>0) & ((data['p_truthOrigin'] == 14)) &
                                (np.abs(data['p_truth_pdgId'][:]) == 22))
        else:
            print(f'tag unknown: {tag} - write it as Hyy_something')
            print('EXITING!')
            sys.exit()
    else:
        mask_remove_999_energy = data['p_PhotonLHLoose'] | data['p_ElectronLHLoose'] | data['p_MuonLHLoose']

    data = data[mask_remove_999_energy]
    return data

def converter(arguments):
    """
    Process converting standard-format ROOT file to HDF5 file with cell
    content.

    Arguments:
        path: Path to the ROOT file to be converted.
        args: Namespace containing command-line arguments, to configure the
            reading and writing of files.

    Returns:
        Converted data in numpy array format
    """
    # sys.exit()
    global args
    import root_numpy
    # Unpack arguments
    index, counter, path, start, stop = arguments
    # Suppress warnings like these when loading the files: TClass::Init:0: RuntimeWarning: no dictionary for class [bla] is available
    ROOT.gErrorIgnoreLevel = ROOT.kError
    log.info("####### 1 #######")

    # Split indexes into 10 sets. # if 10  *** Break *** segmentation violation
    # N = int(np.abs(start-stop)/5000)
    N=10#stop-start
    print(N)
    index_edges = list(map(int, np.linspace(start, stop, N+1, endpoint=True)))
    index_ranges = zip(index_edges[:-1], index_edges[1:])
    # Read-in data from ROOT.TTree
    all_branches = root_numpy.list_branches(path, args.tree) # args.tree
    GNN =False
    keep_branches = keep_branchesMC() #sorted(list(set(all_branches)-set(remove)))
    if GNN:
        keep_branches += GNN_variables()
        # keep_branches = np.array(keep_branches)[['Lr' not in i for i in keep_branches]]
    # print(keep_branches)
    one_evts_array = []
    for i, (loop_start, loop_stop) in tqdm(enumerate(index_ranges), disable = not (start == 0), total = N):
        array = root_numpy.root2array(path, args.tree, start=loop_start, stop=loop_stop, selection=args.selection, branches = keep_branches, warn_missing_tree=True)

        ROOT.gErrorIgnoreLevel = ROOT.kInfo


        n_evts = len(array)
        # If NO events survived, it's probably the selection
        if n_evts == 0:
            return
        # If only one event survives ( can happen with small files) the tracks can't be saved properly???, for now add all of these and save them later
        if n_evts == 1:
            one_evts_array.append(array)
            continue
        # Convert to HDF5-ready format.
        data = convert_to_hdf5(array)
        data = remove_bad_events(data)

        if (args.tree == 'el_tree') and args.datatype == 'MC':
            scale = scale_eventWeight(data['mcChannelNumber'][0])
            data['event_totalWeight'] *= scale

        # Save output of every subprocess to a file
        filename = f'{args.tag}_{index}_{counter}.h5'
        if i == 0:
            saveToFile(filename, data)
        else:
            appendToFile(filename, data)

        del data, array
        gc.collect()

    # Add all arrays with only one event and save them to the output file
    if len(one_evts_array) > 1:
        one_evts_array = np.concatenate(one_evts_array)
        one_evts_data = convert_to_hdf5(one_evts_array)
        filename = '{:s}_{:04d}.h5'.format(args.tag, index)
        appendToFile(filename, one_evts_data)


def scale_eventWeight(MCChannelNumber):
    root_file = ROOT.TFile(f"/groups/hep/ehrke/storage/NtuplefromGrid/Hists/v06Combined/hist.mc.combined.{MCChannelNumber}.root")
    hist = root_file.Get("SumOfWeights")

    scale = ( hist.GetBinContent(4) / hist.GetBinContent(1) ) / hist.GetBinContent(2)
    root_file.Close()
    del root_file
    return scale


def convert_types_to_hdf5(x):
    """
    Variable-length containers are converted to `h5py.special_dtype` with the appropriate `vlen` type.

    Arguments:
        x: Numpy-type variable to be type-checked.

    Returns:
        Same numpy type for scalars and h5py dtype for variable-length containers.
    """
    if 'ndarray' in str(type(x)):
        try:
            return h5py.special_dtype(vlen=convert_types_to_hdf5(x[0]))
        except:
            return h5py.special_dtype(vlen=np.float32)
    elif isinstance(x, str):
        return h5py.special_dtype(vlen=str)
    else:
        return x.dtype



def convert_to_hdf5(data):
    """
    Method to convert standard array to suitable format for classifier.

    Arguments:
        data: numpy array returned by root_numpy.tree2array, to be formatted.

    Returns:
        Flattened numpy recarray prepared for saving to HDF5 file.
    """

    # Format output as numpy structured arrays.
    formats = [convert_types_to_hdf5(data[0][var]) for var in data.dtype.names]
    for pair in zip(data.dtype.names, formats):
        try:
            np.dtype([pair])
        except:
            print("Problem for {}".format(pair))
    dtype  = np.dtype(list(zip(data.dtype.names, formats)))
    output = np.array([tuple([d[var] for var in data.dtype.names]) for d in data], dtype=dtype)

    return output


def imagesToArray(data, var):
    """
    Converts the images retrieved from the ROOT tree into the shape that is
    needed to feed them to keras
    """
    # out = []
    # for i in range(data.shape[0]):
    #     out.append(np.array([a.tolist() for a in data[i]]))
    if 'tile' in str(var):
        out = [np.array(i.tolist()) if np.array(i.tolist()) != [] else np.ones((7,11))*-999 for i in data]
    else:
        out = [np.array(i.tolist()) if np.array(i.tolist()) != [] else np.ones((56,11))*-999 for i in data]
    return np.array(out)


def saveToFile(fname, data):
    """
    Simply saves data to fname.

    Arguments:
        fname: filename (directory is taken from script's args).
        data: numpy array returned by convert_to_hdf5(), or parts hereof.

    Returns:
        Nothing.
    """
    log.info("  Saving to {}".format(args.outdir + fname))
    with h5py.File(args.outdir + fname, 'w') as hf:
        for var in data.dtype.names:
            # Images need to be treated slightly different
            # print(var)

        # ['em_barrel_Lr0', 'em_barrel_Lr1', 'em_barrel_Lr2', 'em_barrel_Lr3'],
        # ['em_endcap_Lr0', 'em_endcap_Lr1', 'em_endcap_Lr2', 'em_endcap_Lr3'],
        # ['time_em_endcap_Lr1', 'time_em_endcap_Lr2', 'time_em_endcap_Lr3'],
        # ['time_em_barrel_Lr0', 'time_em_barrel_Lr1', 'time_em_barrel_Lr2', 'time_em_barrel_Lr3']):
            if (('em' in var.split('_') and 'barrel' in var.split('_')) | ('em' in var.split('_') and 'endcap' in var.split('_')) |
                ('tile' in var.split('_') and 'barrel' in var.split('_')) | ('tile' in var.split('_'))):
                array = imagesToArray(data[f'{var}'], str(var))
                # print(var)
                # print(array)
                # print(array.shape)
                if len(array.shape) != 3:
                    print('Dropping sections - might be error with', var, array.shape)
                    # print(array)
                    continue
                # print(var, array.shape)
                hf.create_dataset( f'{var}', data=array, chunks=True, maxshape= (None, None, None) , compression='lzf' )
                del array
            # Do not save the variable 'BC_bunchIntensities' or cell information
            elif var == 'BC_bunchIntensities':
                continue
            # Create the dataset and save the array
            else:
                # if ("p_cell_" in var):
                #     print(var)
                try:
                    hf.create_dataset( f'{var}', data=data[f'{var}'], chunks=True, maxshape= (None,) , compression='lzf' )
                except (TypeError, KeyError):
                    #log.info(f'Problem with variable: {var}')
                    continue



def appendToFile(fname, data):
    """
    Simply appends data to fname.

    Arguments:
        fname: filename (directory is taken from script's args).
        data: numpy array returned by convert_to_hdf5(), or parts hereof.

    Returns:
        Nothing.
    """
    #log.info("  Appending to {}".format(args.outdir + fname))
    with h5py.File(args.outdir + fname, 'a') as hf:
        for var in data.dtype.names:
            array = data[f'{var}']
            # Images need to be treated slightly different
            if (('em' in var.split('_') and 'barrel' in var.split('_')) | ('em' in var.split('_') and 'endcap' in var.split('_')) |
                ('tile' in var.split('_') and 'barrel' in var.split('_')) | ('tile' in var.split('_'))):
                array = imagesToArray(array, var)
                if len(array.shape) != 3:
                    continue
            # Do not save the variable 'BC_bunchIntensities' or cell information
            if (var == 'BC_bunchIntensities') or ('lar_endcap' in var): # problem with em_endcap_Lr0 and other, it is because they are not added to previous statment
                continue

            # Resize the existing dataset and save the array at the end
            else:
                try:
                    hf[f'{var}'].resize((hf[f'{var}'].shape[0] + array.shape[0]), axis = 0)
                    hf[f'{var}'][-array.shape[0]:] = array
                except (TypeError, KeyError):
                    #log.info(f'Variable missing: {var}')
                    continue



#============================================================================
# Main
#============================================================================

# Make a pool of processes (this must come after the functions needed to run over since it apparently imports __main__ here)
pool = multiprocessing.Pool(processes=args.max_processes)
previous = 0
for path in tqdm(args.paths, total = len(args.paths)):
    print('#####', path, '#####')
    # Count which file we have made it to
    counter += 1

    # Stop if we've reached the maximum number of files
    if args.max_input_files > 0 and counter >= args.max_input_files:
        break

    # Suppress warnings like these when loading the files: TClass::Init:0: RuntimeWarning: no dictionary for class [bla] is available
    ROOT.gErrorIgnoreLevel = ROOT.kError

    # Read ROOT data from file
    # #log.info("Read data from file: {}".format(path))
    f = ROOT.TFile(path, 'READ')

    tree = f.Get(args.tree) # args.tree

    if tree.GetEntries() == 0: continue

    ROOT.gErrorIgnoreLevel = ROOT.kInfo

    # Set the number maximum number of entries to get to args.stop, but only if it's higher than the actual amount of events
    N = min(args.stop, tree.GetEntries())
    log.info("{}: N= {}".format(counter, N))
    # Split indices into equally-sized batches
    # if counter==0:
    index_edges = list(map(int, np.linspace(0, N, args.max_processes + 1, endpoint=True)))
    index_ranges = zip(index_edges[:-1], index_edges[1:])
    # else:
    #     index_edges = list(map(int, np.linspace(previous+1, previous+N, args.max_processes + 1, endpoint=True)))
    #     index_ranges = zip(index_edges[:-1], index_edges[1:])
    # # Start conversion process(es) of ROOT data into numpy arrays and save them
    # #log.info(index_edges)
    # continue
    if not local_test:
        results = pool.map(converter, [(i, counter, path, start, stop) for i, (start, stop) in enumerate(index_ranges)])
    else:
        for i, (start, stop) in enumerate(index_ranges):
            print('##### multi processing #####')
            results = converter((i, counter, path, start, stop))
    previous+=N
log.info('Running event to run function - this will take time')

if args.datatype == 'MC':
    isMC=True
    #['train', 'test', 'val']
    train_test_valid1 = [0.6, 0.2, 0.2]
else:
    isMC=False
    #['train', 'test', 'val']
    train_test_valid1 = [0, 1, 0]
# run_event2row(isMC=isMC, train_test_valid1 = train_test_valid1, folder_tag=args.tag) # chose input variables in from_event2row.py
sec = timedelta(seconds=time() - t_start)
log.info(f"Program finished. Time spent: {str(sec)}")
#%%
if False:
    import glob
    import pandas as pd
    import h5py
    files = glob.glob('output/root2hdf5/Zee_2500e_combine_atlas_reco/*')

    evt = []
    for i in files:
        t = h5py.File(i)
        for k in t.keys():
            if (k == 'test') & (len(t[k]) != 0):
                print(np.sum(t[k]['ATLAS_mass'][:] != 0))



    # _, idx, count = np.unique(evt,return_index=True,return_counts=True)
